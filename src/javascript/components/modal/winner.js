import { showModal } from './modal'
import { createFighterImage } from '../fighterPreview'

export function showWinnerModal (fighter) {
  const title = `Winner is ${fighter.name}`
  const bodyElement = createFighterImage(fighter)
  const onClose = () => document.location.reload()
  showModal({ title, bodyElement, onClose })
}

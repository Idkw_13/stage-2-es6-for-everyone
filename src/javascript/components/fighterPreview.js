import { createElement } from '../helpers/domHelper'

export function createFighterPreview (fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left'
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  })
  if (fighter) {
    const fighterImage = createFighterImage(fighter)
    fighterElement.append(fighterImage)
    const descriptions = createFighterDescription(fighter)
    fighterElement.append(descriptions)
    return fighterElement
  } else {
    return ''
  }
}

function createFighterDescription (fighter) {
  const { name, health, attack, defense } = fighter
  const descriptionContainer  = createElement({
    tagName: 'div',
    className: 'fighter-preview___content'
  })
  const fighterName = createElement({
    tagName: 'div',
    className: 'fighter-preview___text'
  })
  fighterName.innerText = `Name: ${name}`
  const fighterHealth = createElement({
    tagName: 'div',
    className: 'fighter-preview___text'
  })
  fighterHealth.innerText = `Health: ${health}`
  const fighterAttack = createElement({
    tagName: 'div',
    className: 'fighter-preview___text'
  })
  fighterAttack.innerText = `Attack: ${attack}`
  const fighterDefense = createElement({
    tagName: 'div',
    className: 'fighter-preview___text'
  })
  fighterDefense.innerText = `Defense: ${defense}`
  descriptionContainer.append(fighterName)
  descriptionContainer.append(fighterHealth)
  descriptionContainer.append(fighterAttack)
  descriptionContainer.append(fighterHealth)
  descriptionContainer.append(fighterDefense)
  return descriptionContainer
}

export function createFighterImage (fighter) {
  const { source, name } = fighter
  const attributes = {
    src: source,
    title: name,
    alt: name
  }
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  })

  return imgElement
}

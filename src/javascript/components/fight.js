import { controls } from '../../constants/controls'

export async function fight (firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const game = {
      first: {
        status: 'Attack',
        hadCritical: true,
        timeFromListCritical: new Date().getTime(),
      },
      second: {
        status: 'Attack',
        hadCritical: true,
        timeFromListCritical: new Date().getTime(),
      },
    }
    let pressed = new Set()

    document.addEventListener('keydown', function (event) {
      const { first, second } = game
      pressed.add(event.code)
      if (event.code === controls.PlayerOneBlock) {
        first.status = 'Defense'
      }
      if (event.code === controls.PlayerTwoBlock) {
        second.status = 'Defense'
      }

      if (event.code === controls.PlayerOneAttack) {
        first.status = 'Attack'
        if (second.status !== 'Defense') {
          secondFighter.health -= getDamage(firstFighter, secondFighter)
          decreaseIndicator('firstFighter', secondFighter.health)
        }
      }

      if (event.code === controls.PlayerTwoAttack) {
        second.status = 'Attack'

        if (first.status !== 'Defense') {
          firstFighter.health -= getDamage(secondFighter, firstFighter)
          decreaseIndicator('secondFighter', firstFighter.health)
        }
      }

      if (controls.PlayerOneCriticalHitCombination.find(code => code === event.code)) {
        for (let code of controls.PlayerOneCriticalHitCombination) {
          if (!pressed.has(code)) {
            return
          } else {
            checkCritical([first, second])
          }
        }
        if (first.hadCritical) {
          const date = new Date()
          first.timeFromListCritical = date.getTime()
          //disable critical hit
          first.hadCritical = false
          secondFighter.health -= firstFighter.attack * 2
          //show current health in indicator
          decreaseIndicator('firstFighter', secondFighter.health)
        }
        pressed.clear()
      }
      if (controls.PlayerTwoCriticalHitCombination.find(code => code === event.code)) {
        for (let code of controls.PlayerTwoCriticalHitCombination) {
          if (!pressed.has(code)) {
            return
          } else {
            checkCritical([first, second])
          }
        }

        if (second.hadCritical) {
          const date = new Date()
          second.timeFromListCritical = date.getTime()
          //disable critical hit
          second.hadCritical = false
          firstFighter.health -= secondFighter.attack * 2
          //show current health in indicator
          decreaseIndicator('secondFighter', firstFighter.health)
        }
        pressed.clear()
      }
      pressed.clear()
      if (firstFighter.health <= 0) {
        resolve(secondFighter)
      } else if (secondFighter.health <= 0) {
        resolve(firstFighter)
      }
    })
    document.addEventListener('keyup', function (event) {
      pressed.delete(event.code)
    })

  })
}

function checkCritical (fighters) {
  fighters.forEach(fighter => {
    if (!fighter.hadCritical) {
      const date = new Date()
      const differenceLastCritical = (date.getTime() - fighter.timeFromListCritical) / 1000
      if (differenceLastCritical >= 10) {
        fighter.hadCritical = true
      }
    }
  })

}

function decreaseIndicator (position, health) {
  if (position === 'firstFighter') {
    document.querySelector('#right-fighter-indicator').style.width = `${health}%`
  } else {
    document.querySelector('#left-fighter-indicator').style.width = `${health}%`
  }
}

function getRandomNumber () {
  return Math.floor((Math.random() * (1 + 1)) + 1)
}

export function getDamage (attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender)
  return Math.sign(damage) !== -1 ? damage : 0
}

export function getHitPower (fighter) {
  return fighter.attack * getRandomNumber()
}

export function getBlockPower (fighter) {
  return fighter.defense * getRandomNumber()
}
